package rw.ac.rca.termOneExam.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.dto.CreateCityDTO;
import rw.ac.rca.termOneExam.repository.ICityRepository;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {
	
	@Mock
	private ICityRepository cityRepositoryMock;
	
	@InjectMocks
	private CityService cityService;
	
	@Test
	public void getById_success() {

		when(cityRepositoryMock.findById((long) 104))
		.thenReturn(Optional.of(new City(104, "Kabuga", 28.0,0.0)));
		assertEquals("Kabuga",cityService.getById(104).get().getName());
		assertEquals(946.3999999999999,cityService.getById(104).get().getFahrenheit());
	}
	@Test
	public void getById_fail() {

		when(cityRepositoryMock.findById((long) 104))
		.thenReturn(Optional.empty());
		assertEquals(Optional.empty(),cityService.getById(104));
	}
	@Test
	public void getAll_success() {

		when(cityRepositoryMock.findAll())
		.thenReturn(Arrays.asList(new City(104, "Masaka", 28.5,0.0),new City(105, "Huye", 28.0,0.0),new City(106, "Karongi", 29.0,0.0)));
		assertEquals("Masaka",cityService.getAll().get(0).getName());
		assertEquals(946.3999999999999,cityService.getAll().get(1).getFahrenheit());
	}
	@Test
	public void getAll_fail() {

		when(cityRepositoryMock.findAll())
		.thenReturn(null);
		assertEquals(null,cityService.getAll());
	}
	
	@Test
	public void existByName_success() {
		when(cityRepositoryMock.existsByName("Kabuga"))
		.thenReturn(true);
		assertEquals(true, cityService.existsByName("Kabuga"));
	}
	@Test
	public void existByName_not() {
		when(cityRepositoryMock.existsByName("Kabuga"))
		.thenReturn(false);
		assertEquals(false, cityService.existsByName("Kabuga"));
	}
	
	@Test
	public void save_success() {
		CreateCityDTO createCityDTO = new CreateCityDTO("Nyaruguru", 24.5);
		City city = new City(createCityDTO.getName(),createCityDTO.getWeather());
		City created_City = new City(104,createCityDTO.getName(),createCityDTO.getWeather(), 0.0);
		when(cityRepositoryMock.save(city))
		.thenReturn(created_City);
		assertEquals(created_City, cityService.save(createCityDTO));
	}
	
	@Test
	public void calculate_fahrenheit_success() {
		City city = new City("Nyaruguru", 28.0);
		assertEquals(946.3999999999999, cityService.calculate_fahrenheit(city));
	}
	
	

}
