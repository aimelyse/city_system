package rw.ac.rca.termOneExam.utils;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import rw.ac.rca.termOneExam.domain.City;
import rw.ac.rca.termOneExam.repository.ICityRepository;
import rw.ac.rca.termOneExam.service.CityService;


import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CityUtilTest {

    @Mock
    private ICityRepository cityRepository;

    @InjectMocks
    private CityService cityService;


    @Test
    public void city_has_not_less_than_40(){
    	List<City> cities = Arrays.asList(
        		new City(104, "Kicukiro", 28.0,0.0),
                new City(105,"Gasabo",18.0,0.0),
                new City(106,"Rubavu",20.0,0));
        when(cityRepository.findAll()).thenReturn(cities);
        assertEquals(false,less_than_10(cityService.getAll()));
    }


    @Test
    public void testMocking(){
        List<City> cities = mock(List.class);
        assertEquals(cities.size(), 0);
        assertNull(cities.get(0));

        cities.add(new City(104, "Nyaruguru", 24.0,0.0));
        cities.add(new City(105, "Kigali", 28.0,0.0));
        assertEquals(cities.size(), 0);

        when(cities.get(0)).thenReturn(new City(104, "Nyaruguru", 24.0,75.2));
        when(cities.size()).thenReturn(3);

        assertEquals(cities.size(), 3);
        assertEquals(cities.get(0).getName(), "Nyaruguru");
    }


    @Test
    public void testSpying(){
        List<City> cities = spy(List.class);
        assertEquals(cities.size(), 0);
        assertNull(cities.get(0));
        
        cities.add(new City(104, "Kamonyi", 29.0,0.0));
        assertEquals(cities.size(), 0);
        verify(cities, times(1)).add(any(City.class));
        when(cities.size()).thenReturn(5);
        assertEquals(cities.size(), 5);

        cities.add(new City(105, "Kigali", 26.0,0.0));
        assertEquals(cities.size(), 5);
    }




    @Test
    public void cityContainsMusanzeAndKigali(){
        List<City> cities = Arrays.asList(
        		new City(104, "Kigali", 28.0,0.0),
                new City(105,"Musanze",18.0,0.0),
                new City(106,"Rubavu",20.0,0.0));
        when(cityRepository.findAll()).thenReturn(cities);

        assertTrue(contains_musanze_and_kigali(cityService.getAll()));
    }
    
    public boolean greater_than_40(List<City> cities){
        for (City city : cities) {
            if(city.getWeather() > 40){
                return true;
            }
        }
        return false;
    }
    
    private boolean less_than_10(List<City> cities) {
        for (City city : cities) {
            if(city.getWeather() < 10){
                return true;
            }
        }
        return false;
    }
    
    private boolean contains_musanze_and_kigali(List<City> cities) {
        boolean exists = false;
        for (City city : cities) {
            if(Objects.equals(city.getName(), "Musanze") || Objects.equals(city.getName(), "Kigali")){
               exists = true;
               break;
            }
        }
        return exists;
    }

}